﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{

    /*
     * 
    class Program
    {
        static void Main(string[] args)
        {
            Foo("123");
            Foo(null);
            Foo("일이삼");
        }

        static void Foo(string data)
        {
            try
            {
                int number = Int32.Parse(data);
                Console.WriteLine("number : {0}", number);

            }

            catch (ArgumentNullException ex)
            {
                Console.WriteLine("ArgumentNullException 처리 : {0}", ex.Message);
            }

            catch(Exception ex)
            {
                Console.WriteLine("Exception처리 : {0}", ex.Message);
            }

        }



    }

    */


    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                ShowMessage("메세지를 출력 합니다.");
                ShowMessage("null");
            }
            catch(ArgumentNullException ex)
            {
                Console.WriteLine("ArgumentNullException 처리 : {0}", ex.Message);
            }
        }
            

        static void ShowMessage(string message)
        {
            if (message == null)
                throw new ArgumentNullException("message");

            Console.WriteLine(message);
        
        }


    }

}
