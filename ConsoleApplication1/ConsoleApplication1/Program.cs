﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{

    public enum Country { Korea, China, Japan};
    /* if문
    class Program
    {
        static void Main(string[] args)
        {
            Country myCountry = Country.Korea;

            if(myCountry == Country.Korea)
            {
                Console.WriteLine("한국");
            }
            else if (myCountry == Country.Japan)
            {
                Console.WriteLine("일본");

            }
            else if (myCountry == Country.China)
            {
                Console.WriteLine("중국");

            }
            else
            {
                Console.WriteLine("선택된 나라가 없습니다.");
            }
        }
            
    }
    */

        //switch 문
    class Program
    {
        static void Main(string[] args)
        {
            Country myCountry = Country.Korea;

            switch (myCountry)
            {
                case Country.Korea:
                    Console.WriteLine("한국");
                    break;
                case Country.China:
                    Console.WriteLine("중국");
                    break;
                case Country.Japan:
                    Console.WriteLine("일본");
                    break;
                default:
                    Console.WriteLine("선택된 나라가 없습니다.");
                    break;
            }
        }
    }

        }
