﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{

    /* for문 
     
    class Program
    {
        static void Main(string[] args)
        {
            int result = 0;
                
                for(int i = 1; i <=100; i++)
            {
                if (i > 10)
                {
                    break;
                }
                if ((i % 2) == 0)
                {
                    continue;
                }

                result += i;

            }

            Console.WriteLine("break-countinue 1~10중 홀수만 더하기 : {0}", result);
        }
    }

    */



        //   goto문

    class Program
    {
        static void Main(string[] args)
        {
            int result = 0;

            for (int i = 1; i <= 100; i++)
            {
               
                result += i;

                if (i == 5)
                {
                    goto Jump;
                }


            }

            Console.WriteLine("break-countinue 1~10중 홀수만 더하기 : {0}", result);

Jump:
            Console.WriteLine("goto 점프 : {0}", result);
        }
    }

    



}
